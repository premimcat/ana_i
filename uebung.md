# Blatt 1
$4+2+2+2+2+1+4=13$

$2+4+1=7$

## 1

## 2
### a
$5 +(15-\sqrt{2})i -i^2 3\sqrt{2} = 5-3\sqrt{2} - (15-\sqrt{2})i$

### b


### c

### d
$\left(\frac{1+i}{\sqrt{2}}\right)^{2019} = \left(\frac{1}{\sqrt{2}} + \frac{i}{\sqrt{2}}\right)^{2019} = (\frac{1}{2} + 2 \frac{1}{2} i + \frac{1}{2}i^2) \cdot \left(\frac{1+i}{\sqrt{2}}\right)^{2017} = i \cdot \left(\frac{1+i}{\sqrt{2}}\right)^{2017} = i^2 \cdot (\frac{1+i}{\sqrt{2}})^{2015} = -(\frac{1+i}{\sqrt{2}})^{2015} = (\frac{1+i}{\sqrt{2}})^3 = \frac{i-1}{\sqrt{2}}$

## 3

## 4
Die zwei Moeglichkeiten sind:
Die Koenigin sagt vorher, dass Rumpelstielzchen ihr das Kind zurueck gibt.
Wenn Rumpelstielzchen ihr das Kind zurueck gibt, hat sie richtig vorhergesagt und sie kriegt ihr Kind zurueck.
Wenn Rumpelstielzchen ihr das Kind nicht zurueck gibt, dann hat sie falsch vorhergesagt und es zu recht nicht zurueck bekommen.

Die Koenigin sagt vorher, dass Rumpelstielzchen ihr das Kind nicht zurueck gibt.
Wenn Rumpelstielzchen ihr das Kind nicht zurueck gibt, dann hat sie dies richtig prognostiziert, also sollte sie ihr Kind zurueck kriegen. Dies fuerht zu einem Paradoxon.
Wenn Rumpelstielzchen ihr das Kind zurueck gibt, dann hat sie dies falsch prognostiziert, also sollte sie ihr Kind nicht zurueck kriegen. Dies fuerht auch zu einem Paradoxon.

Im zweiten Fall kann sie nicht richtig vorhersagen, da alle Vorhersagen zu einem Paradoxon fuehren.

Wir wuerden der Koenigin die erste Variante empfehlen, da ansonsten ihr Kind explodiert und so hat die Koenigin wenigstens eine Chance das Kind wieder zu bekommen.

### 2. versuch (der richtige)
Koenigin sagt vorraus, dass sie das Kind zurueck kriegt oder nicht.

Wenn sie das Kind zurueck kriegt, dann hat sie richtig vorhher gesagt und kriegt das Kind zurueck.
Wenn nicht, dann hat sie auch richtig vorher gesagt und sie sollte es eigentlich auch zurueck kriegen, doch dies führt zu einem Paradoxon.

## 5
Aussage in Logik:

$(A \land B \land C \Leftrightarrow \neg D) \land (\neg (\neg A \land \neg B) \lor (\neg D \lor C))$

$(A \land B \land C \Leftrightarrow \neg D) \land (\neg A \land \neg B \land D \land \neg C)$

Negierung und ein paar mal DeMorgan und anderes umformen. Die wichtigsten Regeln, die wir verwenden:

DeMorgan $\neg(X \lor Y) \Leftrightarrow \neg X \land \neg Y$

Implikation $\neg X \land Y \Leftrightarrow X \rightarrow Y$

\begin{align*}
	&\neg \big((A \land B \land C \Leftrightarrow \neg D) &&\land (\neg (\neg A \land \neg B) &&\lor (\neg D \lor C))\big) \\
	&\neg (A \land B \land C \Leftrightarrow \neg D) &&\lor \neg (\neg (\neg A \land \neg B) &&\lor (\neg D \lor C)) \\
	&\neg ((A \land B \land C \land \neg D) \lor (\neg (A \land B \land C) \land D))  &&\lor ((\neg A \land \neg B) &&\land (D \land \neg C)) \\
	&((A \land B \land C \land \neg D) \lor (\neg (A \land B \land C) \land D)) &&\rightarrow (\neg A \land \neg B &&\land\phantom{(}D \land \neg C\phantom{(}) \\
%	&((\neg A \lor \neg B \lor \neg C \lor D) \land (A \lor B \lor C \lor \neg D))  &&\lor ((\neg A \land \neg B) &&\land (D \land \neg C)) \\
%	&\neg((A \land B \land C \land \neg D) \lor (A \neg \land \neg B \land \neg C \land D))  &&\lor ((\neg A \land \neg B) &&\land (D \land \neg C)) \\
%	&((A \land B \land C \land \neg D) \lor (A \neg \land \neg B \land \neg C \land D))  &&\rightarrow (\neg A \land \neg B &&\land D \land \neg C) \\
\end{align*}

Wenn Albert, Benjamin, und Clemens, jedoch nicht Daniel zur Party kommen oder Albert, Benjamin oder Clemens nicht zu party erscheinen, wohl aber Daniel, dann kommen Albert, Benjamin und Clemens nicht zur Party, Daniel aber schon.
Also kann es nicht auftreten, dass nicht D kommt, aber A, B, und C schon.

### 2. versuch
\begin{tabular}{c|cccc}
		&-	&C	&DC	&D	\\
	-	&1	&1	&0	&1	\\
	A	&1	&1	&0	&0	\\
	AB	&1	&0	&1	&0	\\
	B	&1	&1	&0	&0	\\
\end{tabular}

$(\neg B \land \neg D) \lor (\neg A \land \neg D) \lor (\neg C \land \neg D) \lor (\neg A \land \neg B \land \neg C) \lor (A \land B \land C \land D)$


Wenn Daniel nicht erscheint, dann erscheint auch einer der anderen nicht.
Wenn Daniel erscheint, dann erscheint sonst niemand oder alle.


## 6
Wir haben die Laengen des ganzen Stocks $1$, des laengeren Stocks $l$ und des kuerzeren $k$. Gefragt sind $\ell$ und $k$.

Wir wissen:

$1 = \ell +k$

$\frac{1}{\ell}=\frac{\ell}{k}$


### Wir setzen fuer $k$, $1-l$ ein

$\frac{1}{\ell} = \frac{\ell}{1-\ell} \Leftrightarrow 1 = \frac{\ell^2}{1-\ell} \Leftrightarrow 1-\ell = \ell^2 \Leftrightarrow 0 = \ell^2 + \ell -1$


pq-Formel:

$\ell_{1, 2} = -\frac{1}{2} \pm \sqrt{\frac{5}{4}} = -\frac{1}{2} \pm \frac{\sqrt{5}}{2} \Leftrightarrow \ell_{1, 2} = \phi - \frac{1}{2} \pm \frac{1}{2}$ 

Da $\phi \geq 1$, $\ell = \phi -1 \approx 0.61$ und $k = 1-\phi$.

## 7
### a
$f'= 6x-5$

$x^a$ mit $a\geq 1, a\in \mathbb{N}$ leiten wir mit $ax^{a-1}$ ab.
Eine Konstante geht bei der Ableitung ``verloren''.
Wenn eine Funktion mehrere Terme hat, dann leiten wir diese einzeln ab.

### b


## 8
Sei die Strasse oBdA die x-Achse.
Sei auserdem oBdA $D=(D_x,D_y)=(0, 1)$.
Wir definieren $D':=(D_x,-D_y)=(0,-1)$ als die Spiegelung von $D$ an der x-Achse.
Für jeden Punkt $p'$ auf der Strasse ist die Strecke $\overline{Dp'}$ ist gleich lang wie die Strecke $\overline{D'p'}$, da $|\overline{Dp'}| = \sqrt{(D_x-p'_x)^2 + (D_y-p'_y)^2} = \sqrt{(0-p'_x)^2 + (1-0)^2} = \sqrt{(p'_x)^2 + 1} = \sqrt{(p'_x)^2 + (-1)^2} = |\overline{D'p^*}|$.
Da wir im $\mathbb{R}^2$ sind (haben wir beschlossen), gilt die Dreiecksungleichung. Daher ist $|\overline{D'G}| \leq |\overline{D'p'}| + |\overline{p'G}| = |\overline{Dp'}| + |\overline{p'G}|$ für jedes $p' \in \mathbb{R}^2$.
Da $\overline{D'G}$ die Strasse kreuzt (da $D'$ per Definition auf der anderen Seite der Strasse liegt als $G$), gibt es einen Schnittpunkt $p''$ zwischen $\overline{D'G}$ und der Strasse.
Daher haben wir nun einen Lösungskandidaten ($p''$) und wir wissen aus der Dreiecksungleichung, dass es keinen Punkt $p'''\in \text{Strasse}$ geben kann, sodass $|\overline{D'p'''}| + |\overline{p'''G}| < |\overline{D'p''}| + |\overline{p''G}| = |\overline{D'G}|$.
Daher ist die gesuchte Summe für $p''$ minimal.
